package AngajatiApp.model;

import org.junit.*;

import static org.junit.Assert.*;

    public class EmployeeTest {
        private Employee e1;
        private Employee e2;
        private Employee e3;
        private Employee e4;
        private Employee e5;
        //se executa automat inaintea fiecarei metode de test
        @Before
        public void setUp() {
            e1 = new Employee();
            e1.setFirstName("Radu");
            e2 = new Employee();
            e2.setLastName("Muresan");
            e3 = new Employee();
            e3.setCnp("1234567891011");
            e4 = new Employee();
            e4.setId(11);
            e5 = null;
            System.out.println("Before test");
        }

        //se executa automat dupa fiecare metoda de test
        @After
        public void tearDown() {
            e1 = null;
            e2 = null;
            e3 = null;
            e4 = null;
            System.out.println("After test");
        }

        @Test
        public void testGetFirstName() {
            assertEquals("Radu", e1.getFirstName());
        }

        @Test
        public void testGetLastName() {
            assertEquals("Muresan", e2.getLastName());
        }
        @Ignore
        @Test
        public void testGetCnp() {
            assertEquals("1234567891011", e3.getCnp());
        }
        @Test
        public void testGetId(){
            assertEquals(11, e4.getId());
        }
        @Test
        public void testConstructor() {
            assertNotEquals("check for creating employee e1", e1, null);
        }

        @BeforeClass
        public static void setUpAll() {
            System.out.println("Before all tests - at the beginning of the Test class");
        }

        @AfterClass
        public static void tearDownAll() {
            System.out.println("After all tests - at the end of the Test class");
        }
        @Test (expected=NullPointerException.class)
        public void testGetId2() {
            assertEquals(11, e5.getId());
        }
            @Test(timeout = 100) //asteapta 10 milisecunde
        public void testFictiv() {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }